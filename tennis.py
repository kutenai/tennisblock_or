#!/usr/bin/env python
"""
Initial play/test project to run tennisblock
"""

import argparse
import yaml
from os.path import exists

# Note: this is generally a bad idea.. but, to simplify things *for now*, it's cool
from pulp import *

def do_work(players=None):
    """
    A function to do the work in..
    :return:
    """
    men = players.get('men',[])
    women = players.get('women',[])

    print("Scheduling with {} men and {} women.".format(
        len(men), len(women)
    ))

    # Now for the hard part..



def main():
    """
    Main runner, with argument parsing..
    :return:
    """
    parser = argparse.ArgumentParser(description='Tennis Scheduler.')

    parser.add_argument('players_file',
        help='First required argument is a file of players.')

    parser.add_argument('--arg1', '-a',
        help='Example of an optional arg.. called arg1')

    args = parser.parse_args()

    if exists(args.players_file):
        with open(args.players_file,'r') as fp:
            players = yaml.load(fp)
        print("Loaded players.")
    else:
        sys.exit("Players file ({}) does not exist.".format(args.players_file))

    #print("Argument 1, named 'arg1' is {}".format(args.arg1))

    do_work(players=players)


if __name__ == "__main__":
    main()