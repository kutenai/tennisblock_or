Readme file for Tennisblock_or

# Introduction

Start working on code to perform tennis group optimization. The current front runner for technology is Pulp.


# Installation

Start with a working Python version, version 2.7 preferred. I currently have 2.7.8

	> python -V
	Python 2.7.8
	
If you have an older version of Python installed, then you should upgrade. I don't recommend Homebrew or Macports.. just install native. Stick with 2.7.8 for now.. 

Here is a reference that looks pretty good. http://wolfpaulus.com/jounal/mac/installing_python_osx/

## Python Packages

Now that you have the proper updated python version, we want to insure we have the package manager (PIP) and virtual environment setup.
	
Insure you have the proper setup tools installed. namely, pip. The quick way to do this is with easy_install. 

	sudo easy_install pip
	
Now you should be ready to setup your virtual environments.. these are pretty important in Python.

First, install virtualenv, this is pretty easy now that you have pip

	sudo pip install virtualenv
	sudo pip install virtualenvwrapper
	
Now you have virtualenv, and virtualenvwrapper installed in the ROOT python installation. From now on, you only install packages in a project specific virtualenv.

## Setup VirtualEnv Wrapper

You need a place to store you virtualenv's.. 

	mkdir ~/.virtualenvs
	
Now, updates you ~/.bashrc to include a few shortcuts

	export WORKON_HOME=~/.virtualenvs
	source /usr/local/bin/virtualenvwrapper.sh
	
Note: Make sure that virtualenvwrapper.sh is intalled at that location. Sometimes, it does somewhere else, like /usr/bin.. look around if you can't find it.

# Using VirtualEnv

To create a new virtualenv, you use the mkvirtualenv command.

	mkvirtualenv tennis_or
	
This will create an isolated environment for that project. From now on, if you open a new shell and want to work in the environment, you use the 'workon' command.

	workon tennis_or
	
You can setup some fancy bash completions to allow you to <tab> autocomplete that.. I love those. 

## Updating your virtualenv.

Normally a Python project will include a 'requirements.txt' file. this is a PIP install file that installs required packages and versions. TO update you virutlaenv for a project, just do this:

	workon tennis_or
	pip install -r requirements.txt
	
That assumes you are CD'ed into the proper directory and requirements.txt is at the root of the project, which is typical. 

## Adding stuff to requirements.txt

If you find some new module you now require for the project, just 'pip install <modulename>' in our virtualenv. To let anyone else workng on the project know that this module is now required, you need to update the requirements.txt file. This is easy.

	pip freeze --local >| requirements.txt
	
pip freeze displays all installed modules. --local shows only those in the local VM.. and you overwrite the requirements with the new stuff. Commit all to git, and life is groovy.


	
